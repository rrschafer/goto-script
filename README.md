# goto script
This script is meant for saving and accessing locations you switch between in your CLI usage without having to remember and type their names out everytime. The general usage is this:
- `goto` will show a list of saved directories and prompt the user to choose which one to `cd` to
- `goto .` will save the current directory to the list of available directories for future `goto`ing
- `goto -c` will clear the list of available directories
- `goto -r` will show a list of available directories and prompt the user which one to remove from the list
- `goto -h` shows a help, much like this readme
The directory list is saved by default at `$HOME/.goto_locations`.

To set up the script to work system-wide, the usual steps apply:
- Make it executable `chmod +x goto.sh`
- Add its location to the path `PATH=PATH:/path/to/goto.sh`
- Or set up an alias in your `.bashrc` or `.zshrc` like `alias goto="/path/to/goto.sh"` 
- Or just `alias goto="source /path/to/goto.sh"` directly