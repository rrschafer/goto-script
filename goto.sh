#!/usr/bin/sh

goto_file="$HOME/.goto_locations"

case $1 in
    .)
		if [ -f $goto_file ]; then
			while read comppath; do
				currentpath=$(pwd)
				if [[ $comppath == $currentpath ]]; then
					echo "Current directory already available for goto."
					return 0
				fi
			done <$goto_file
		fi
		pwd >> $goto_file
		echo "Current directory saved for future goto."
		return 0
    ;;
    -c)
		if [ -f $goto_file ]; then
			rm $goto_file
			echo "goto directories cleared."
			return 0
		else
			echo "goto directories were already cleared."
		fi
    ;;
	-h)
		echo "This CLI tool is meant to save directories you will want to return to. The options are:"
		echo " - goto    \t lets you choose from a list of directories to go to"
		echo " - goto .  \t adds the current directory to the list if not there yet"
		echo " - goto -c \t clears the list of directories"
		echo " - goto -r \t lets you choose a directory to remove from the list"
		echo " - goto -h \t shows this help"
	;;
    -r)
		if [ ! -f $goto_file ]; then
			echo "There are currently no directories to remove from the goto list."
			return 0
		fi

		echo "Which directories do you wish to remove from goto?"
		counter=1

		while read newpath; do
			echo -e "$counter\t $newpath"
			let counter=$counter+1
		done <$goto_file
		read which
		if [ $which -le $counter ]; then
			newcounter=1
			while read newpath; do
				if [[ $which == $newcounter ]]; then
					echo "Removing $newpath"
					sed -i "$newcounter d" $goto_file
					# if file empty now, remove
					if [[ counter -eq 2 ]]; then
						rm $goto_file
					fi
					return 0
				fi
				let newcounter=$newcounter+1
			done <$goto_file
		else
			echo "The value $which is not a valid option."
			return 0
		fi

    ;;
    *)
		if [ ! -f $goto_file ]; then
			echo "There are currently no directories to goto."
			return 0
		fi

		echo "Which directories do you wish to go to?"
		counter=1

		while read newpath; do
			echo -e "$counter\t $newpath"
			let counter=$counter+1
		done <$goto_file
		read which
		if [ $which -le $counter ]; then
			newcounter=1
			while read newpath; do
				if [[ $which == $newcounter ]]; then
					echo "Going to $newpath"
					cd $newpath
					return 0
				fi
				let newcounter=$newcounter+1
			done <$goto_file
		else
			echo "The value $which is not a valid option."
			return 0
		fi
    ;;
esac

return 0
